import React, {Component} from 'react';

import UserService from '../services/user.service';

import '../css/card.css';

export default class Home extends Component {
  constructor (props) {
    super (props);

    this.state = {
      detail: {},
    };
  }

  componentDidMount () {
    UserService.getAll ().then (
      response => {
        this.setState ({
          detail: response.data,
        });
      },
      error => {
        this.setState ({
          content: (error.response && error.response.data) ||
            error.message ||
            error.toString (),
        });
      }
    );
  }

  render () {
    const {detail} = this.state;
    console.log (detail);
    return (
      <div>
        {detail &&
          detail.length > 0 &&
          detail.map (detail => (
            <div className="grid">
              <div className="grid-item">
                <div className="card">
                  <div className="card-content">
                    <h1 className="card-header">{detail.username}</h1>
                    <p className="card-text">
                      {detail.email}
                    </p>
                    <p className="card-text">
                      {detail.role}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
    );
  }
}
